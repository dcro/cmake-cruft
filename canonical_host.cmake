# attempt to find a consistent name for the host (the architecture on which
# the code will execute)
macro (canonical_host)
    if (NOT DEFINED host_cpu)
        message (STATUS "checking the host CPU")

        # we have a sane compiler, so ask it directly for the architecture.
        if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
            execute_process (
                COMMAND ${CMAKE_CXX_COMPILER} -dumpmachine
                RESULT_VARIABLE __canonical_host_result
                OUTPUT_VARIABLE __canonical_host_output
            )

            if (${__canonical_host_result})
                message (FATAL_ERROR "unable to query for canonical host")
            endif (${__canonical_host_result})

            string(REGEX MATCH "^([^-\]+)" host_cpu ${__canonical_host_output})
        elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
            set (host_cpu "x86_64")
        else ()
            message (FATAL_ERROR "unknown compiler")
        endif ()

        message (STATUS "checking the host CPU - found '${host_cpu}'")
    endif ()
endmacro (canonical_host)
