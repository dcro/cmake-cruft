if (NOT AUTOCONF_DEFINES)
    add_definitions(-DPACKAGE="${PROJECT_NAME}")
    add_definitions(-DPACKAGE_VERSION="${PROJECT_VERSION}")
    add_definitions(-DPACKAGE_NAME="${PROJECT_NAME}")
    add_definitions(-DPACKAGE_STRING="${PROJECT_NAME} ${PROJECT_VERSION}")

    set (AUTOCONF_DEFINES 1)
endif()

# Unconditionally link pthreads in. Some projects won't need it, but most
# will in some capacity if only through implied dependency (like thread pools).
set (THREADS_PREFER_PTHREAD_FLAG ON)
find_package (Threads REQUIRED)
link_libraries (Threads::Threads)

include (nc_cxx)
include (nc_sanitizer)
include (nc_platform)
include (nc_optimisation)
include (nc_warnings)
