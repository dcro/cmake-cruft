macro(test_restrict VARIABLE)
    if (NOT DEFINED TEST_${VARIABLE})
        message (STATUS "checking for restrict keyword")
        FOREACH(KEYWORD "restrict" "__restrict" "__restrict__" "_Restrict")
            IF (NOT TEST_${VARIABLE})
                TRY_COMPILE(
                    TEST_${VARIABLE}
                    "${CMAKE_BINARY_DIR}/CMakeTmp"
                    SOURCES "${CMAKE_SOURCE_DIR}/cmake/test_restrict.cpp"
                    COMPILE_DEFINITIONS "-DKEYWORD=${KEYWORD}")
                set(__test_restrict_last ${KEYWORD})
            ENDIF (NOT TEST_${VARIABLE})
        endforeach(KEYWORD)

        if (TEST_${VARIABLE})
            message (STATUS "checking for restrict keyword - ${__test_restrict_last}")
            set(${VARIABLE} ${__test_restrict_last} CACHE INTERNAL "restrict keyword" FORCE)
        else (TEST_${VARIABLE})
            message (STATUS "checking for restrict keyword - not found")
        endif(TEST_${VARIABLE})
    endif(NOT DEFINED TEST_${VARIABLE})
endmacro(test_restrict)
