###############################################################################
if (__nc_platform)
    return ()
endif ()

set (__nc_platform TRUE)


###############################################################################
include (compile_flag)
include (link_flag)


###############################################################################
# attempt to detect platforms that aren't super common
if (${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
    set(LINUX 1)
elseif (${CMAKE_SYSTEM_NAME} STREQUAL "FreeBSD")
    set(FREEBSD 1)
elseif (WIN32)
else()
    message (FATAL_ERROR "unknown platform")
endif()


###############################################################################
# apply useful, non-obvious, default settings for each platform
if (WINDOWS)
    add_definitions("-DWIN32_LEAN_AND_MEAN")
endif(WINDOWS)

if (FREEBSD)
    append_compile_flag("-I/usr/local/include")
    append_link_flag("-L/usr/local/lib")
endif (FREEBSD)


###############################################################################
# general platform assistance settings
append_link_flag("-fno-common")
append_link_flag("-fno-nonansi-builtins")
