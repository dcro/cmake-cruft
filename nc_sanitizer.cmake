option (SANITIZER "enable sanitizers" OFF)

if (SANITIZER)
    append_compile_flag("-fsanitize=undefined")
    append_compile_flag("-fsanitize=address")
    append_compile_flag("-fsanitize=leak")

    append_compile_flag("--param max-gcse-memory=167772160")

    append_compile_flag("-fsanitize=pointer-compare")
    append_compile_flag("-fsanitize=pointer-subtract")

    add_definitions("-D_GLIBCXX_SANITIZE_VECTOR")
endif()