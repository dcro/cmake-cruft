include(CheckCXXSourceCompiles)
include(CMakeCheckCompilerFlagCommonPatterns)

# Test if a compiler flag is supported.
#
# Note: We _cannot_ use check_cxx_compiler_flag because it will not add the
# flag to the linker parameters and some compiler flags _must_ be passed to
# the linker but the test it performs fails to do so. eg, checking for
# -fsanitize=address is impossible with this macro.
#
# Instead, we roll out own. Yay cmake...
macro (append_compile_flag _flag)
    string (MAKE_C_IDENTIFIER ${_flag} _name)

    CHECK_COMPILER_FLAG_COMMON_PATTERNS(_append_compile_flag_COMMON_PATTERNS)
    set(_append_compile_flag_COMMON_PATTERNS
        ${_append_compile_flag_COMMON_PATTERNS}
        FAIL_REGEX "argument unused during compilation" #clang
    )

    if (NOT DEFINED compile_flag_${_name})
        # If we have an argument of the form "-Wno-foo" then we can't test it
        # directly as GCC will unconditioally succeed right up until the
        # point any other warning or error is triggered and *then* dump a
        # warning message.
        #
        # Instead you have to test for -Wfoo and only if that succeeds can
        # you use the negation.
        string (REGEX MATCH "^-Wno-(.+)$" compile_flag_inverse ${_flag})

        set(_append_compile_flag_REQUIRED_FLAGS "${CMAKE_REQUIRED_FLAGS}")

        if ("x" STREQUAL "x${compile_flag_inverse}")
            set(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS} ${_flag}")
        else ()
            string (REGEX REPLACE "-Wno-" "" _inverse_flag ${_flag})
            set(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS} -W${_inverse_flag}")
        endif ()

        check_cxx_source_compiles(
            "int main(int,char**) { return 0; }"
            compile_flag_${_name}
            ${_append_compile_flag_COMMON_PATTERNS}
        )

        set(CMAKE_REQUIRED_FLAGS "${_append_compile_flag_REQUIRED_FLAGS}")
    endif ()

    if (compile_flag_${_name})
        set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${_flag}")
    endif()
endmacro(append_compile_flag)
