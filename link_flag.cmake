###############################################################################
macro(check_link_flag _variable _flag)
    string (MAKE_C_IDENTIFIER ${_flag} _name)

    if (NOT DEFINED __check_link_flag_${_name})
        message (STATUS "checking linker flag ${_flag}")

        set (__check_linker_flag_old "${CMAKE_EXE_LINKER_FLAGS}")
        set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${_flag}")

        try_compile (
            __check_link_flag_${_name}
            "${CMAKE_CURRENT_BINARY_DIR}/CMakeTmp"
        SOURCES
            "${CMAKE_CURRENT_SOURCE_DIR}/cmake/test_link_flag.cpp")

        if (__check_link_flag_${_name})
            message (STATUS "checking linker flag ${_flag} - found")
        else()
            message (STATUS "checking linker flag ${_flag} - not found")
        endif()

        set (CMAKE_EXE_LINKER_FLAGS "${__check_linker_flag_old}")
    endif ()

    set (${_variable} ${__check_link_flag_${_name}})
endmacro()


##-----------------------------------------------------------------------------
macro(append_link_flag _flag)
    check_link_flag (__test_link_flag_${_flag} ${_flag})
    if (__test_link_flag_${_flag})
        set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${_flag}")
    endif()
endmacro()
