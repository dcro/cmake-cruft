typedef int * int_ptr;
int foo (int_ptr KEYWORD ip) {
    return ip[0];
}

int
main (int argc, char **argv){
    (void)argc;
    (void)argv;

    int s[1];
    int * KEYWORD t = s;
    t[0] = 0;
    return foo(t);
}
