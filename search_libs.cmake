###############################################################################
## Search a (possibly empty) list of libraries for the library required to link
## against a given (extern C) symbol. Pass the list of libraries as variadic
## arguments to the macro.
##
## Operates analogously to autoconfs AC_SEARCH_LIBS.
##
## If the symbol is found the variable provided will be set to the required
## library, or cleared for no library. Sets variable_FOUND to true if the
## symbol was discovered in some capacity (to differentiate between no library
## required, and the symbol not being found)
##
## Does not work for C++ symbols.

macro(search_libs _variable _symbol)
    if (NOT DEFINED __search_libs_${_symbol})
        message (STATUS "searching for ${_symbol}")

        if (NOT DEFINED __search_libs_${_symbol}_lib)
            try_compile(
                __search_libs_${_symbol}
                "${CMAKE_BINARY_DIR}/CMakeTmp"
            SOURCES
                "${CMAKE_SOURCE_DIR}/cmake/search_libs.cpp"
            COMPILE_DEFINITIONS
                "-DSYMBOL=${_symbol}")

            if (__search_libs_${_symbol})
                set (__search_libs_${_symbol}_lib CACHE INTERNAL "library exposing ${_symbol}")
            else ()
                foreach (lib ${ARGN})
                    if (NOT __search_libs_${_symbol})
                        message (STATUS "searching for ${_symbol} in ${lib}")

                        try_compile(
                            __search_libs_${_symbol}
                            "${CMAKE_BINARY_DIR}/CMakeTmp"
                        SOURCES
                            "${CMAKE_SOURCE_DIR}/cmake/search_libs.cpp"
                        LINK_LIBRARIES
                            "${lib}"
                        COMPILE_DEFINITIONS
                            "-DSYMBOL=${_symbol}")

                        if (__search_libs_${_symbol})
                            set (__search_libs_${_symbol}_lib ${lib} CACHE INTERNAL "library exposing ${_symbol}")
                        endif ()
                    endif ()
                endforeach()
            endif ()
        endif ()

        if (__search_libs_${_symbol})
            message (STATUS "searching for ${_symbol} - found")
        else ()
            message (STATUS "searching for ${_symbol} - not found")
        endif()
    endif ()

    set (${_variable}_FOUND ${__search_libs_${_symbol}})
    set (${_variable} ${__search_libs_${_symbol}_lib})
endmacro()
