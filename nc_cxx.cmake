###############################################################################
if (__nc_cxx)
    return ()
endif ()

set (__nc_cxx TRUE)

###############################################################################
# clear the default flags because they include optimisation flags we
# explicitly do not want (ie, O3).
set (CMAKE_CXX_FLAGS_RELEASE "")
set (CMAKE_C_FLAGS_RELEASE "")
set (CMAKE_CXX_FLAGS_DEBUG "")
set (CMAKE_C_FLAGS_DEBUG "")


###############################################################################
include (compile_flag)
include (test_restrict)


###############################################################################
append_compile_flag ("-fcolor-diagnostics")


###############################################################################
append_compile_flag ("-std=c++2a")
append_compile_flag ("-fpermissive")
append_compile_flag ("/std:c++latest")

# try to use clangs libc++ under the assumption that gcc will fail this test
# and clang will pass (if it is present). this is necessary because some
# libstdc++ headers don't behave well under c++17/clang.
append_compile_flag ("-stdlib=libc++")

###############################################################################
# find the gcc or clang filesystem library and append to libs if needed
#
# note, we specifically _cannot_ use find_library because it just makes wild
# hardcoded guess and doesn't respect _all_ the system paths; specifically
# something like /usr/lib/gcc/x86_64-pc-linux-gnu/6.3.0/libstdc++.so won't get
# discovered.
if (NOT DEFINED __nc_cxx_stdcxxfs)
    foreach(lib "" "c++fs" "c++experimental" "stdc++fs")
        if (NOT __nc_cxx_stdcxxfs)
            message (STATUS "Trying c++fs library '${lib}'")
            try_compile(
                __nc_cxx_stdcxxfs ${CMAKE_CURRENT_BINARY_DIR}
            SOURCES
                ${CMAKE_CURRENT_SOURCE_DIR}/cmake/nc_cxx_stdcxxfs.cpp
            LINK_LIBRARIES
                ${lib})

            if (__nc_cxx_stdcxxfs)
                set (__nc_cxx_stdcxxfs "${lib}" CACHE INTERNAL "library required for c++ filesystem")
            endif ()
        endif ()
    endforeach()

    if (NOT __nc_cxx_stdcxxfs)
        message (FATAL_ERROR "Could not locate the required c++fs library")
    endif ()

    message (STATUS "Using c++fs library ${__nc_cxx_stdcxxfs}")
endif ()

set(NC_CXX_STDCXXFS ${__nc_cxx_stdcxxfs})


###############################################################################
test_restrict(RESTRICT_KEYWORD)
add_definitions("-Drestrict=${RESTRICT_KEYWORD}")


###############################################################################
append_compile_flag("-pipe")
append_compile_flag("-fno-deduce-init-list")
append_compile_flag("-fvisibility=hidden")
append_compile_flag("-fPIC")

append_compile_flag("-D__STDC_FORMAT_MACROS")

add_definitions("-D_FILE_OFFSET_BITS=64")
