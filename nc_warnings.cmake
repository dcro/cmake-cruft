###############################################################################
if (__nc_warnings)
    return ()
endif ()

set (__nc_warnings TRUE)


###############################################################################
include (compile_flag)


###############################################################################
append_compile_flag(-Wall)
append_compile_flag(-Wextra)

append_compile_flag(-Wno-pragmas)


# -------------------------------------------------------------------------
# General warnings
append_compile_flag(-Wbool-compare)
append_compile_flag(-Wcast-align)
append_compile_flag(-Wcast-qual)
append_compile_flag(-Wdisabled-optimization)
append_compile_flag(-Wfloat-conversion)
append_compile_flag(-Wfloat-equal)
append_compile_flag(-Wno-aggressive-loop-optimizations)
#append_compile_flag(-Wnoexcept) # Wow this can be annoying when combined with Werror
append_compile_flag(-Wnon-virtual-dtor)
append_compile_flag(-Wparentheses) # Wparenthesis detects assignment in conditionals...
append_compile_flag(-Wpointer-arith)

# redundant-decls would be useful, but it specifically clashes with GCC
# intrinsics under msys2 where the builtin and headers define them with
# different linkages.
#append_compile_flag(-Wredundant-decls)

append_compile_flag(-Wshadow)
append_compile_flag(-Wsign-compare)
append_compile_flag(-Wsign-promo)
append_compile_flag(-Wstrict-aliasing)
#append_compile_flag(-Wstrict-overflow)
append_compile_flag(-Wtype-limits)
append_compile_flag(-Wunused-but-set-variable)
append_compile_flag(-Wunused-parameter)
append_compile_flag(-Wpessimizing-move)
# Don't use -Wswitch-enum, it's the same as -Wswitch but also warns if you use
# a default. Just don't use a default if that's something you want to catch.
append_compile_flag(-Wswitch)

# dont use unsafe-loop-optimisations as there are too many false positives
# with language primitives like range-for.
# append_compile_flag(-Wunsafe-loop-optimizations)

# gcc-6.1.0 warnings
append_compile_flag(-Wshift-negative-value)
append_compile_flag(-Wnull-dereference)
append_compile_flag(-Wduplicated-cond)

# useless-cast isn't usable on gcc-6.0 or gcc-6.1 due to spurious warnings
# see gcc#70844
append_compile_flag(-Wuseless-cast)

# gcc 7 warnings
append_compile_flag(-Wduplicated-branches)
append_compile_flag(-Wrestrict)
append_compile_flag(-Wregister)
append_compile_flag(-Wdangling-else)
append_compile_flag(-Walloc-zero)
append_compile_flag(-Walloca)
append_compile_flag(-Wnonnull)
append_compile_flag(-Waligned-new)

append_compile_flag(-Wno-implicit-fallthrough)

# clang 3.7.1 warnings
append_compile_flag(-Wshorten-64-to-32)
append_compile_flag(-Wdeprecated)
append_compile_flag(-Wcovered-switch-default)
append_compile_flag(-Wcovered-switch-default)

append_compile_flag(-Wno-gnu-string-literal-operator-template)

# gcc 8.1

append_compile_flag(-Wmultistatement-macros)
append_compile_flag(-Wcast-align=strict)
append_compile_flag(-Wold-style-cast)


# -------------------------------------------------------------------------
# Required extensions
#AX_APPEND_COMPILE_FLAGS([-Wgnu-flexible-array-union-member], [], [-Werror])
#AX_APPEND_COMPILE_FLAGS([-Wno-c99-extensions], [], [-Werror])
#AX_APPEND_COMPILE_FLAGS([-Wno-vla-extension], [], [-Werror])
append_compile_flag(-Wno-vla)
append_compile_flag(-Wno-multichar)

#AS_IF([test "x$ax_cv_cxx_compiler_vendor" != "xgnu"], [
#    AX_APPEND_COMPILE_FLAGS([-Wno-c99-extensions], [], [-Werror])
#    AX_APPEND_COMPILE_FLAGS([-Wno-flexible-array-extensions], [], [-Werror])
#    AX_APPEND_COMPILE_FLAGS([-Wno-gnu-conditional-omitted-operand], [], [-Werror])
#    AX_APPEND_COMPILE_FLAGS([-Wno-gnu-empty-struct], [], [-Werror])
#    AX_APPEND_COMPILE_FLAGS([-Wno-gnu-flexible-array-union-member], [], [-Werror])
#    AX_APPEND_COMPILE_FLAGS([-Wno-gnu-zero-variadic-macro-arguments], [], [-Werror])
#    AX_APPEND_COMPILE_FLAGS([-Wno-vla-extension], [], [-Werror])
#    AX_APPEND_COMPILE_FLAGS([-Wno-zero-length-array], [], [-Werror])
#])

# -------------------------------------------------------------------------
# Excessive warnings
append_compile_flag(-Wno-missing-braces)

#AS_IF([test "x$ax_cv_cxx_compiler_vendor" != "xgnu"], [
#    AX_APPEND_COMPILE_FLAGS([-Wno-nested-anon-types], [], [-Werror])
#    AX_APPEND_COMPILE_FLAGS([-Wno-unused-const-variable], [], [-Werror])
#])

# -------------------------------------------------------------------------
option (ADVISORY "enable advisory warnings")

if (ADVISORY)
    append_compile_flag(-Winline)
    append_compile_flag(-Wsuggest-final-types)
    append_compile_flag(-Wsuggest-final-methods)
    append_compile_flag(-Wsuggest-override)
    append_compile_flag(-Wweak-vtables)
    append_compile_flag(-Wpadded)
endif()

