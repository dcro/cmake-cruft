cmake_minimum_required (VERSION 3.7)

find_program(ASCIIDOC NAMES asciidoc asciidoctor DOC "path to the asciidoc binary")
mark_as_advanced (ASCIIDOC)

if (ASCIIDOC)
    execute_process (
        COMMAND ${ASCIIDOC} --version
        OUTPUT_VARIABLE ASCIIDOC_version_output
        ERROR_VARIABLE  ASCIIDOC_version_error
        RESULT_VARIABLE ASCIIDOC_version_result
    )

    if (${ASCIIDOC_version_output})
        string (
            REGEX REPLACE
            "^asciidoc (\\d+\\.\\d+\\.\\d+)$"
            "\\1"
            ASCIIDOC_VERSION
            "${ASCIIDOC_version_output}"
        )
    endif ()

    set (ASCIIDOC_target_usage "ASCIIDOC_target(<name> SOURCE <source> OUTPUT <output> [DEPENDS ...])")

    macro (ASCIIDOC_target name)
        set (_ASCIIDOC_${name}_onoff_args HTML5)
        set (_ASCIIDOC_${name}_value_args SOURCE OUTPUT)
        set (_ASCIIDOC_${name}_multi_args DEPENDS)

        cmake_parse_arguments (_ASCIIDOC_${name}
            "${_ASCIIDOC_${name}_onoff_args}"
            "${_ASCIIDOC_${name}_value_args}"
            "${_ASCIIDOC_${name}_multi_args}"
            ${ARGN}
        )

        if (NOT _ASCIIDOC_${name}_SOURCE)
            message (FATAL_ERROR "${ASCIIDOC_target_usage}")
        endif ()

        if (NOT _ASCIIDOC_${name}_OUTPUT)
            message (FATAL_ERROR "${ASCIIDOC_target_usage}")
        endif ()

        get_filename_component (dir "${_ASCIIDOC_${name}_OUTPUT}" DIRECTORY)
        file (MAKE_DIRECTORY "${dir}")

        add_custom_command (
        OUTPUT
            "${_ASCIIDOC_${name}_OUTPUT}"
        COMMAND
            "${ASCIIDOC}" 
            -b html5
            -o "${_ASCIIDOC_${name}_OUTPUT}"
            #--theme=volnitsky
            -a icons
            "${_ASCIIDOC_${name}_SOURCE}"
        DEPENDS
            "${_ASCIIDOC_${name}_SOURCE};${_ASCIIDOC_${name}_DEPENDS}"
        MAIN_DEPENDENCY
            "${_ASCIIDOC_${name}_SOURCE}"
        COMMENT
            "[asciidoc] ${_ASCIIDOC_${name}_SOURCE}"
        )

        add_custom_target (${name} "${_ASCIIDOC_${name}_OUTPUT}")
    endmacro ()
endif ()


include(FindPackageHandleStandardArgs)
find_package_handle_standard_args (ASCIIDOC
    REQUIRED_VARS  ASCIIDOC
    VERSION_VAR    ASCIIDOC_VERSION
)
